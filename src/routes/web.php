<?php

Route::get('notice', function(){
	echo 'This is Notice Route';
});

Route::get('/list_notice', 'Muradbdinfo\Notice\NoticeController@index')->name('list_notice');
Route::get('/add', 'Muradbdinfo\Notice\NoticeController@add');
Route::post('/insert_notice', 'Muradbdinfo\Notice\NoticeController@insert');
Route::get('/edit_notice/{id}', 'Muradbdinfo\Notice\NoticeController@edit');
Route::post('/update_notice/{id}', 'Muradbdinfo\Notice\NoticeController@UpdateNotice');
Route::get('delete_notice/{id}', 'Muradbdinfo\Notice\NoticeController@DeleteNotice');