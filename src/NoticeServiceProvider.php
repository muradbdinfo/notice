<?php

namespace Muradbdinfo\Notice;

use Illuminate\Support\ServiceProvider;


class NoticeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Muradbdinfo\Notice\NoticeController');
        $this->loadViewsFrom(__DIR__.'/views', 'notice');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'./routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'./database/migrations');
    }
}
